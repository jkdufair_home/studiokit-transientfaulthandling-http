﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.TransientFaultHandling.Http.Interfaces
{
	public interface IHttpClient
	{
		Task<string> GetStringAsync(
			Uri uri,
			CancellationToken cancellationToken = default(CancellationToken),
			AuthenticationHeaderValue authorizationHeader = null,
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

		Task<string> PostAsync(
			Uri uri,
			Func<Uri, HttpContent> getHttpContent,
			CancellationToken cancellationToken = default(CancellationToken),
			AuthenticationHeaderValue authorizationHeader = null,
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

		Task<HttpResponseMessage> SendAsync(
			HttpRequestMessage httpRequestMessage,
			CancellationToken cancellationToken = default(CancellationToken),
			AuthenticationHeaderValue authorizationHeader = null,
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null);
	}
}